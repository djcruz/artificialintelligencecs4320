import matplotlib as plt
import os.path,subprocess
from subprocess import STDOUT,PIPE
import csv
from random import randint


def executeJava(fileName, stdin):
	java_class,ext = os.path.splitext(fileName)
	cmd = ['java', java_class] + stdin.split(" ")
	os.chdir("C:\\Users\\dcruz\\git\\artificialIntelligenceCS4320\\Assignment4\\src\\")
	proc = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
	stdout,stderr = proc.communicate()	
	#print ('This was "' + stdout.decode() + '"')
	#return stdout
	return stdout.decode()

def plotIncreasing(start, stop, algor, seeds):
	#holds score and and boolean for if deadline was exceeded. appended to the dataSet
	dataSet = []
	deadlineExceeded = []
	scoreSet = []
	timeSet = []
	for i in range(start, stop):
		stdin = str(i) + " " + str(i) + " " + str(i) + " " + str(10) + " " + algor + " " + seeds[i]
		print(stdin)
		stdout = executeJava("Main.java", stdin)
		#print(stdout)
		print(stdout)
		deadlineExceeded.append(True)
		for line in stdout.split("\n"):
			if "EXCEEDED DEADLINE" in line:
				deadlineExceeded[-1] = False
			if "Score:" in line:
				scoreSet += [(float(line.split(" ")[1]))]
			if "Time remaining:" in line:
				timeSet += [(int(line.split(" ")[2]))]
	dataSet.append(scoreSet)
	dataSet.append(deadlineExceeded)
	dataSet.append(timeSet)
	print(dataSet)
	return dataSet

def listToCSV(input, file):
	wr = csv.writer(file, dialect="excel")
	if type(input[0]) == list:
		for data in input:
			wr.writerow(data)
	else:
		wr.writerow(input)

def writeAxis(start, stop, file):
	output = [i for i in range(start, stop)]
	wr = csv.writer(file, dialect="excel")
	wr.writerow(output)

def getSeeds(start, stop):
	seedList = []
	for i in range(start, stop):
		seedList += str(randint(1,100))
	print(seedList)
	return seedList 

def compareAll(start,stop):

	file = open("data.csv", "w")
	writeAxis(start,stop, file)

	seeds = getSeeds(start, stop)

	for i in range(0,3):
		output = plotIncreasing(start, stop, str(i), seeds)
		listToCSV(output, file)
	file.close()
	print(seeds)

#Used on CSP
def singleRun(start, stop, algor):
	file = open("CSP0.csv", "w")
	writeAxis(start,stop, file)

	seeds = ['9', '9', '8', '7', '6', '1', '9', '6', '7', '9', '6', '1', '8', '3', '6', '6', '2', '9', '1', '1', '5', '2', '2', '2', '5', '1', '2', '2', '0', '3', '3', '8', '1', '4', '3', '7', '3', '6', '4', '1', '6', '6', '2', '8', '3', '7', '9', '0', '4', '1', '9', '4', '4', '5', '0', '2', '1', '9', '9', '4', '3', '1', '9', '8', '2', '1', '3', '4', '0', '5', '1', '2', '7', '8', '3', '8', '3', '8', '6', '4', '3', '2', '5', '1', '0', '5', '8', '2', '1', '7', '9', '1', '9', '4', '4']#getSeeds(start, stop)

	output = plotIncreasing(start, stop, str(algor), seeds)
	listToCSV(output, file)
	file.close()

def plotPopulation(start, stop, algor, seed):
	dataSet = []

	for i in range(start, stop):
		stdin = "90 90 90 60 2 " + seed
		stdout = executeJava("Main.java", stdin)
		#print(stdout)
		print(stdout)
		pop = [ int(i) for i in stdout.split("\n")[:-1]]
		if(len(dataSet) == 0):
			dataSet = pop
		else:
			dataSet = [dataSet[i] + pop[i] for i in range(len(pop))]

	dataSet = [dataSet[i]//(stop - start) for i in range(len(dataSet))]

	print(dataSet)
	return dataSet


def singleRunPopulation(start, stop):
	file = open("genetic.csv", "w")
	writeAxis(start,stop, file)

	#seeds = [str(80),str(20),str(10),str(34),str(21),str(34),str(54),str(32),str(12),str(45)]#getSeeds(start, stop)

	output = plotPopulation(start, stop, str(2), "39")
	listToCSV(output, file)
	file.close()

def main():
	#executeJava("Main.java", "1 3 4 10 0 20") #"1", "3","4","10","2","20")
	start = 1
	stop = 50
	#compareAll(start, stop)
	singleRun(start,stop,2)
	#singleRunPopulation(0,50);


if __name__ == '__main__':
	main()