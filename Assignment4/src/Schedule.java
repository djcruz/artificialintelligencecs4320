import java.util.Collections;
import java.lang.Math;

public class Schedule{
  int[][] schedule;
  double fitness;

  Schedule(int nRooms, int nTimeSlots) {
    schedule = new int[nRooms][nTimeSlots];
  }

  Schedule(int[][] initSchedule){
  	schedule = initSchedule.clone();
  }

  //deep copy
  Schedule(Schedule thatSched){
    schedule = new int[thatSched.schedule.length][thatSched.schedule[0].length];

  	for(int i = 0; i < thatSched.schedule.length; i++){
  		for(int j = 0; j< thatSched.schedule[0].length; j++){
  			this.schedule[i][j] = thatSched.schedule[i][j];

  		}
  	}
  	this.fitness = thatSched.fitness;
  }
}
