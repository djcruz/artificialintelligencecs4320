import java.util.*;

public class Csp{
	ArrayList<Vd> variableSet;
	SchedulingProblem problem; // to hold the search problem
	long deadline;

	Csp(SchedulingProblem newProblem, long newDeadline){
		problem = newProblem;
		deadline = newDeadline;
		variableSet = initSet();
	}

	//run CSP to create schedule
	public Schedule run(){
		Schedule solution = problem.getEmptySchedule();
		recursiveCSP(solution.schedule, new ArrayList<ScheduleSlot>()); 

		return solution;
	}



	//a recusive implementation. assigned keeps a set of all locations already taken
	public int[][] recursiveCSP(int[][] schedule, ArrayList<ScheduleSlot> assigned){
		if(variableSet.isEmpty()){
			return schedule;
		}
	    if(System.currentTimeMillis() > deadline){ //deadline exceeded
	      return schedule;
	    }
		else{
			Vd variable = variableSet.remove(0);
			for(ScheduleSlot slot : variable.domain){
				if(!assigned.contains(slot)){
					assigned.add(slot);
					schedule[slot.room][slot.timeSlot] = problem.courses.indexOf(variable.course);

					Schedule check = problem.getEmptySchedule();
					check.schedule = schedule;

					int[][] result = recursiveCSP(schedule, assigned); //recusion, returns null if failure down the line
					if(result != null){
						return result;
					}
					//free slot
					else{
						assigned.remove(slot);
						schedule[slot.room][slot.timeSlot] = -1;
					}
				}
			}
		//failed, add variable back to set
		variableSet.add(variable);
		}
		return null;
	}

	//parses variable domain pairs and sorts domains
	public ArrayList<Vd> initSet(){
		ArrayList<Course> courses = problem.courses;
		ArrayList<Vd> variableSet = new ArrayList<Vd>();

		//make all courses a variable
		for(Course course : courses){
			variableSet.add(new Vd(problem, course));
		}

		//build domain
		for(int j = 0; j < problem.NUM_TIME_SLOTS; j++){
			for(int i = 0; i < problem.rooms.size(); i++){
				ScheduleSlot slot =  new ScheduleSlot(i, j);

				for(Vd variable : variableSet){
					Course course = variable.course;
					//Hard constrains are enforced, domain will only count if it is a valid time and the room capacity is valid
					if((course.timeSlotValues[j] > 0) && (course.enrolledStudents <= problem.rooms.get(i).capacity)){
						variable.domain.add(slot);
					}
				}
			}

		}

		ArrayList<Vd> toRemove = new ArrayList<Vd>();
		//Hueristic 1, sort by best domain first. 
		for(Vd variable: variableSet){
		 	variable.sortByBest();
		 	if(variable.domain.size() == 0){
		 		toRemove.add(variable);
		 	}
		}

		//Remove any variables without any domains
		for(Vd removeVd : toRemove)
			variableSet.remove(removeVd);
		//Heuristic 2, Sort 
		variableSet.sort(Comparator.reverseOrder());
		return variableSet;
	}

	public void printSchedule(int[][] s){
		for(int i = 0 ; i < s.length; i++){
			for(int j = 0; j < s[i].length; j++){
				System.out.printf("%4d", s[i][j]);
			}
			System.out.println();
		}
	}
}


