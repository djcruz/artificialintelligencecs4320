import java.util.*;



class Genetic{
	ArrayList<Vd> courseSet; //Holds all possible scheduleSlots for each course, for reference
	ArrayList<Member> population;

	long deadline;
	SchedulingProblem problem;

	Genetic(SchedulingProblem newProblem, int popSize, long newDeadline){
		problem = newProblem;
		courseSet = initSet();
		deadline = newDeadline;
		population = randomPopulation(popSize);
	}
	
	//Runs algorithm
	public Schedule run(final int MAXGENERATIONS){
		double fittest = Double.NEGATIVE_INFINITY;
		int staleCount = 0;						//If the same max fitness in a population is constant for 10 generations in a row, end search.
		final int MAXSTALE = 100;
		int popSize = population.size();
		for(int j = 0; j < MAXGENERATIONS; j++){
			if(System.currentTimeMillis() > deadline)
				break;
			for(int i = 0; i < popSize/5; i++){
				Member[] parents = runRouletteWheel();
				Member child = crossover(parents);
				mutate(child);

				Member worse = getWorse();
				population.remove(worse);
				population.add(child);
				setPopFitness();
			}
			double currentBest = getBest().fitness;
		}
		return getBest().schedule;
	}


	public Member[] runRouletteWheel(){
		double totalFitness = setPopFitness();
		Random rn = new Random();
		boolean zeroOut = false;

		Member parent1 = null;
		Member parent2 = null;

		//chances of parents selection
		int parent1Sel = 0;
		int parent2Sel = 0;

		//If negative values need to be considered, add a zero out option
		double worstFitness = getWorse().fitness;
		if(worstFitness < 0){
			zeroOut = true;
			totalFitness = getZeroedTotalFitness(worstFitness);
		}

		//chosen parents must not be the same, roulette will keep running until both parents are chosen.
		while(parent1 == null || parent2 == null){
			if(parent1 == null)
				parent1Sel = rn.nextInt((int) totalFitness + 1);
			if(parent2 == null)
				parent2Sel = rn.nextInt((int) totalFitness + 1);

			double runningFitness = 0;
			for(Member member : population){
				if(zeroOut)
					runningFitness += member.fitness - worstFitness;
				else
					runningFitness += member.fitness;
				if(parent1Sel <= runningFitness && parent1 == null && member != parent2)
					parent1 = member;
				if(parent2Sel <= runningFitness && parent2 == null && member != parent1)
					parent2 = member;
				if(parent1 != null && parent2 != null)
					break;
			}
		}
		Member[] parents = {parent1, parent2};
		return parents;
	}

	//totale fitness of the population
	public double getTotalFitness(){
		double totalFitness = 0;
		for(Member member : population){
			totalFitness += member.fitness;
		}
		return totalFitness;
	}

	//Zero out total fitness if there are negative fitness scores
	public double getZeroedTotalFitness(double worstFitness){
		double totalFitness = 0;
		for(Member member : population){
			totalFitness += member.fitness - worstFitness;
		}
		return totalFitness;
	}

	//50/50 chance to add course in location of one of the two parents.
	public Member crossover(Member[] parents){
		Random rn = new Random();
		Member child = new Member();
		for(Vd variable : courseSet){
			Course course = variable.course;
			Gene gene0 = parents[0].findCourse(course);
			Gene gene1 = parents[1].findCourse(course);
		
			if(gene1 != null && rn.nextInt(10) >= 4){
				child.addGene(gene1);
			}
			else if(gene0 != null){
				child.addGene(gene0);
			}
		}
		return child;
	}

	//add or move a course to the best course location
	public void mutate(Member member){
		Random rn = new Random();
		for(Vd variable : courseSet){
			if(rn.nextInt(100) < 15){ //15% change
				Gene mutateGene = member.findCourse(variable.course);
				ScheduleSlot bestSlot = variable.domain.get(0);
				if(mutateGene != null){
					member.removeGene(mutateGene);
					Gene clobberGene = member.getGeneAtLocation(bestSlot);
					if(clobberGene != null)
						member.removeGene(clobberGene);
					mutateGene.location = bestSlot;
					member.addGene(mutateGene);
				}
				else{
					Gene clobberGene = member.getGeneAtLocation(bestSlot);
					if(clobberGene != null)
						member.removeGene(clobberGene);
					mutateGene = new Gene(variable.course, bestSlot);
					member.addGene(mutateGene);
				}
			}
		}
	}

	//sets and saves fitness of each member, returns total fitness if necessary
	public double setPopFitness(){
		double totalFitness = 0;
		for(Member member : population){
			totalFitness += member.setFitness();
		}
		return totalFitness;
	}


	//should be ran after calculating fitness
	public Member getWorse(){
		ArrayList<Member> sortedPop = new ArrayList<Member>(population);
		Collections.sort(sortedPop);
		return sortedPop.get(0);
	}

	public Member getBest(){
		ArrayList<Member> sortedPop = new ArrayList<Member>(population);
		Collections.sort(sortedPop);		
		return sortedPop.get(population.size() - 1);
	}

	//Chooses a random populcation based in the courseSet domain for each course
	public ArrayList<Member> randomPopulation(int popSize){
		population = new ArrayList<Member>(popSize);
		Random rn = new Random();
		for(int i = 0; i < popSize; i++){
			population.add(randomMember());
		}
		return population;
	}

	//Generates a randomized Member
	public Member randomMember(){
		Member member = new Member(problem);
		Random rn = new Random();
		ArrayList<ScheduleSlot> assignedSlots = new ArrayList<ScheduleSlot>();
		int giveUp = 20; //give up after 20 trys of attempting to put course in

		for(Vd variable : courseSet){
			ScheduleSlot slot;
			int domainSize = variable.domain.size();

			if(domainSize > 0){
					
					int tryCount = 0;
					do{
						slot = variable.domain.get(rn.nextInt(domainSize));
						tryCount++;
					}
					while(assignedSlots.contains(slot) && tryCount < giveUp);
	
					if(tryCount < giveUp){
						assignedSlots.add(slot);
						Gene gene = new Gene(variable.course, slot);
						member.addGene(gene);
					}
				}
		}
		return member;
	}



	//parses variable domain pairs and sorts domains
	public ArrayList<Vd> initSet(){
		ArrayList<Course> courses = problem.courses;
		ArrayList<Vd> variableSet = new ArrayList<Vd>();

		//make all courses a variable
		for(Course course : courses){
			variableSet.add(new Vd(problem, course));
		}

		//build domain
		for(int j = 0; j < problem.NUM_TIME_SLOTS; j++){
			for(int i = 0; i < problem.rooms.size(); i++){
				ScheduleSlot slot =  new ScheduleSlot(i, j);

				for(Vd variable : variableSet){
					Course course = variable.course;
					//Hard constrains are enforced, domain will only count if it is a valid time and the room capacity is valid
					if((course.timeSlotValues[j] > 0) && (course.enrolledStudents <= problem.rooms.get(i).capacity)){
						variable.domain.add(slot);
					}
				}
			}
		}

		ArrayList<Vd> toRemove = new ArrayList<Vd>();
		for(Vd variable: variableSet){
		 	variable.sortByBest();
		 	if(variable.domain.size() == 0){
		 		toRemove.add(variable);
		 	}
		}
		//Remove any variables without any domains
		for(Vd removeVd : toRemove)
			variableSet.remove(removeVd);
		variableSet.sort(Comparator.reverseOrder());
		return variableSet;
	

	}

	public void printPopulation(){
		ArrayList<Member> sortedPop = new ArrayList<Member>(population);
		Collections.sort(sortedPop);	
		for(Member member : sortedPop){
			System.out.println(problem.evaluateSchedule(member.schedule));
		}
	}
}


//A single member of the population, constains the schedule, an arrayList of schedule locations, and the fitness of the schedule
class Member implements Comparable<Member>{
	Schedule schedule;
	ArrayList<Gene> dna;
	double fitness;

	static SchedulingProblem problem;
	Member(SchedulingProblem newProblem){
		problem = newProblem;
		schedule = problem.getEmptySchedule();
		dna = new ArrayList<Gene>();
	}

	Member(){
		schedule = problem.getEmptySchedule();
		dna = new ArrayList<Gene>();
	}

	Member(Schedule newSchedule, ArrayList<Gene> newDna){
		schedule = newSchedule;
		dna = newDna;
	}
	Member(Member member){
		dna = member.dna;
		fitness = member.fitness;
		Schedule schedule = new Schedule(member.schedule);
	}

	public void setProblem(SchedulingProblem newProblem){
		problem = newProblem;
	}
	//Add a course with its location
	public void addGene(Gene newGene){
		ScheduleSlot location = newGene.location;
		schedule.schedule[location.room][location.timeSlot] = problem.courses.indexOf(newGene.course);
		dna.add(newGene);
	}
	//remove a course with its location
	public void removeGene(Gene oldGene){
		ScheduleSlot location = oldGene.location;
		schedule.schedule[location.room][location.timeSlot] = -1;
		dna.remove(oldGene);
	}
	//return gene based on course
	public Gene findCourse(Course course){
		for(Gene gene : dna){
			if(gene.course == course)
				return gene;
		}
		return null;
	}
	//returns gene based on location in a gene
	public Gene getGeneAtLocation(Gene locationGene){
		for(Gene gene : dna){
			if(gene.location == locationGene.location)
				return gene;
		}

		return null;
	}

	//returns a gene based on location as a ScheduleSlot
	public Gene getGeneAtLocation(ScheduleSlot location){
		for(Gene gene : dna){
			if(gene.location == location)
				return gene;
		}

		return null;
	}

	public Double setFitness(){
		fitness = problem.evaluateSchedule(schedule);
		return fitness;
	}

 	@Override
   	public int compareTo(Member thatMember){
  	if(fitness > thatMember.fitness)
  		return 1;
  	if(fitness == thatMember.fitness){
  		return 0;
  	}
  	return -1;
   	}
}


//contains course and it's location in the schedule
class Gene{
	Course course;
	ScheduleSlot location;
	Gene(Course newCourse, ScheduleSlot newLocation){
		course = newCourse;
		location = newLocation;
	}
}

