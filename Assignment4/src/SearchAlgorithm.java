import java.util.*;
import java.lang.Math;

public class SearchAlgorithm {


  /**************************************** constraint satisfaction Algorithm ************************************************/
  //check for bactracking to see given course timeslot is appropriate

  // backtracking with constraint satisfaction
  public Schedule constraintSatisfaction(SchedulingProblem problem, long deadline) {

    // get an empty solution to start from
    Schedule solution = problem.getEmptySchedule();

    Csp csp = new Csp(problem, deadline);

    return csp.run();
  }

  /****************************Genetic Algorithm********************************/
    // Your search algorithm should return a solution in the form of a valid
  // schedule before the deadline given (deadline is given by system time in ms)

  public Schedule genetic(SchedulingProblem problem, long deadline) {

     final int POPSIZE = 50;
     final int MAXGENERATIONS = 1000;

    // get an empty solution to start from
    Schedule solution = problem.getEmptySchedule();

    Genetic genetic = new Genetic(problem, POPSIZE, deadline);
    
    solution = genetic.run(MAXGENERATIONS);

    return solution;
  }


  // This is a very naive baseline scheduling strategy
  // It should be easily beaten by any reasonable strategy
  public Schedule naiveBaseline(SchedulingProblem problem, long deadline) {

    // get an empty solution to start from
    Schedule solution = problem.getEmptySchedule();

    for (int i = 0; i < problem.courses.size(); i++) {
      Course c = problem.courses.get(i);
      boolean scheduled = false;
      for (int j = 0; j < c.timeSlotValues.length; j++) {
        if (scheduled) break;
        if (c.timeSlotValues[j] > 0) {
          for (int k = 0; k < problem.rooms.size(); k++) {
            if (solution.schedule[k][j] < 0) {
              solution.schedule[k][j] = i;
              scheduled = true;
              break;
            }
          }
        }
      }
    }

    return solution;
  }
}
