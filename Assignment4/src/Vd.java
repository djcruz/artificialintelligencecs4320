import java.util.*;

//Variable-Domain capsule
class Vd implements Comparable<Vd>{
	Course course; //variable
	ArrayList<ScheduleSlot> domain; //domain set
	SchedulingProblem problem;

	Vd(SchedulingProblem newProblem, Course newCourse, ArrayList<ScheduleSlot> newDomain){
		course = newCourse;
		domain = newDomain;
		problem = newProblem;
	}

	Vd(SchedulingProblem newProblem, Course newCourse){
		course = newCourse;
		domain = new ArrayList<ScheduleSlot>();
		problem = newProblem;
	}

	private double evaluateSlot(ScheduleSlot slot){
		return course.timeSlotValues[slot.timeSlot] - problem.DISTANCE_PENALTY * getDistance(course.preferredLocation, problem.rooms.get(slot.room).b);
	}

	private double getDistance(Building b1, Building b2){
		    double xDist = (b1.xCoord - b2.xCoord) * (b1.xCoord - b2.xCoord);
	        double yDist = (b1.yCoord - b2.yCoord) * (b1.yCoord - b2.yCoord);
	        return Math.sqrt(xDist + yDist);
	}

	//Find out how valuable each value is based on time and building distance and sort.
	public void sortByBest(){
		ArrayList<ScheduleSlot> sortedDomain = new ArrayList<ScheduleSlot>(domain.size());
		for(ScheduleSlot slot : domain){
			boolean isSorted = false;
			for(int i = 0; i < sortedDomain.size() && !isSorted; i++){
				double slotValue =  evaluateSlot(slot);
				if(evaluateSlot(sortedDomain.get(i)) < slotValue){
					sortedDomain.add(i, slot);
					isSorted = true;
				}
			}
			if(!isSorted)
				sortedDomain.add(slot);
		}
		domain = sortedDomain; 
	}

	//variables based on best.
	@Override
  	public int compareTo(Vd thatVd){
  		double thisDomAve = 0;
  		double thatDomAve = 0;
  		for(ScheduleSlot value : domain){
  			thisDomAve += evaluateSlot(value) + course.value;
  		}
  		thisDomAve /= domain.size();
  		for(ScheduleSlot value: thatVd.domain){
  			thatDomAve += evaluateSlot(value) + thatVd.course.value;
  		}
  		thatDomAve /= thatVd.domain.size();

  	if(thisDomAve > thatDomAve)
  		return 1;
  	if(thisDomAve == thatDomAve){
  		return 0;
  	}
  	return -1;
  	}

}


//basic building block of the domain set
class ScheduleSlot{
	int room;
	int timeSlot;

	ScheduleSlot(int newRoom, int newTimeSlot){
		room = newRoom;
		timeSlot = newTimeSlot;
	}

}