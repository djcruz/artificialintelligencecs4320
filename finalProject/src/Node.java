//Neural node
import java.util.*;

class Node{
	public float val;
	public float bias;
	public float[] weights;
	public Node[] next;

	public float[] deltaWeights;
	public float deltaBias;

	Node(){}

	Node(float newBias){
		bias = newBias;
	}

	void connect(Layer nextLayer){
		int layerSize = nextLayer.nodes.length;
		next = new Node[layerSize];
		weights = new float[layerSize];
		deltaWeights = new float[layerSize];

		for (int i = 0; i < layerSize; i++) {
			next[i] = nextLayer.nodes[i];
		}
	}
}