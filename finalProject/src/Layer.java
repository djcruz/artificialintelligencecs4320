//single layer for a network
import java.util.*;
import java.lang.Math.*;


class Layer{
	Node[] nodes;
	//float[][] weights;
	Layer nextLayer;
	Layer prevLayer;


	Layer(int size){
		nodes = new Node[size];
		for(int i = 0; i < nodes.length; i++){
			nodes[i] = new Node();
		}
	}

	//sets
	void connect(Layer connectLayer){
		nextLayer = connectLayer;
		connectLayer.prevLayer = this;
		for(int i = 0; i < nodes.length; i++){
			nodes[i].connect(connectLayer);
		}
	}

	//sets all weights and biases in a layer to random
	void reset(){
		Random random;
		random = new Random();
		//for each node set bias to random
		for(int i = 0; i < nodes.length; i++){
			nodes[i].bias = random.nextFloat();
			//set all weights to 0
			float[] weights = nodes[i].weights;
			//last layer will not have weights
			if(weights != null){
				for(int j = 0; j < weights.length; j++){
					weights[j] = random.nextFloat();
				}
			}
		}
	}

	//alter the weights of the next layer
	void feedForward(){
		Node[] nextNodes = nextLayer.nodes;

		//for each nodes in next layer
		for(int j = 0; j < nextNodes.length; j++){
			nextNodes[j].val = 0;
			nextNodes[j].val = nextNodes[j].bias;
			//for each nodes in this layer
			for(int i = 0; i < this.nodes.length; i++){
				nextNodes[j].val += this.nodes[i].val * this.nodes[i].weights[j];
			}
		
			//sigmoid squish
			nextNodes[j].val = sigmoid(nextNodes[j].val);
		}
	}

	//takes in delta of the next layers
	public float[] backprop(float[] deltas, float ETA, float ALPHA){
		Node[] nextNodes = nextLayer.nodes;
		float[] prevDelta = null;
		//calculate deltas for previous layer
		if(this.prevLayer != null){
			prevDelta = new float[prevLayer.nodes.length];
			for(int i = 0; i < nodes.length; i++){
				for(int j = 0; j < nextNodes.length; j++){
					prevDelta[i] += nodes[i].weights[j] * deltas[j];
				}
			}
			//update bias
			//check if I'm doing the correct delta TODO
			for(int i = 0; i < nodes.length; i++){
				this.nodes[i].deltaBias = ETA * prevDelta[i] + ALPHA * this.nodes[i].deltaBias;
				this.nodes[i].bias += this.nodes[i].deltaBias;
			}
		}
		//update weights
		for(int i = 0; i < nodes.length; i++){
			for(int j = 0; j < nextNodes.length; j++){
				this.nodes[i].deltaWeights[j] = ETA * this.nodes[i].val * deltas[j] + ALPHA * this.nodes[i].deltaWeights[j];
				this.nodes[i].weights[j] += this.nodes[i].deltaWeights[j];
			}
		}

		// if(this.prevLayer != null){

		// }


		return prevDelta;

	}
	
	//sigmoid funciton
	float sigmoid(float x){
		return (float) (1.0/(1.0 + Math.exp(-x)));
	}

}