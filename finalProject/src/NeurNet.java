import java.util.*;

public class NeurNet{
	public ArrayList<Layer> layers;
	
	NeurNet(){
		layers = new ArrayList<Layer>();
	}

	public void addLayer(Layer nextLayer){
		int numLayers = layers.size();
		if(numLayers > 0){
			layers.get(numLayers - 1).connect(nextLayer);
			layers.add(nextLayer);
		}
		else
			layers.add(nextLayer);

	}

	//reset all layers
	public void reset(){
		for (Layer layer : layers) {
			layer.reset();
		}
	}

	//run forward propogation
	public void run(float[] input){
		mount(input);
		for (Layer layer : layers) {
			if(layer.nextLayer == null)
				break;
			layer.feedForward();
		}
	}

	//run and return predicted value
	public int predict(float[] input){
		run(input);
		Layer lastLayer = layers.get(layers.size() - 1);
		Node[] output = lastLayer.nodes;

		int prediction = 0;
		for(int i = 0; i < output.length; i++){
			if(output[i].val > output[prediction].val)
				prediction = i;
		}
		return prediction;
	}


	//inputs input to first layer
	public void mount(float[] input){
		Node[] inputNodes = layers.get(0).nodes;
		for(int i = 0; i < inputNodes.length; i++){
			inputNodes[i].val = input[i];
		}
	}

	//calculates delta of output layers
	public float[] getGradient(float[] expectedOutput){
		Layer lastLayer = layers.get(layers.size()-1);
		Node[] nodes = lastLayer.nodes;
		float[] deltas = new float[nodes.length];

		for(int i = 0; i < nodes.length; i++){
			deltas[i] = 2*(expectedOutput[i]-nodes[i].val)*nodes[i].val*(1-nodes[i].val);
		}
		return deltas;
	}

	//Learn on a dataset and labels
	public void learn(float[][] data, float[][] labels){
		float ALPHA = (float)0.3;
		float  ETA = (float)0.1;
		//for each image and label sample
		for(int s = 0; s < data.length; s++){
			this.run(data[s]);
			float[] deltas = getGradient(labels[s]);
			Layer currLayer = layers.get(layers.size()-2);
			while(currLayer != null){
				deltas = currLayer.backprop(deltas, ALPHA, ETA);
				currLayer = currLayer.prevLayer;
			}
		}
	}



	public void print(){
		for(Layer layer : layers){
			for(Node node : layer.nodes){
				System.out.print("bias: " + node.bias + " ");
				if(node.weights != null)
					for(int i = 0; i < node.weights.length; i++){
						System.out.printf("weight[%d]=%f ", i, node.weights[i]);
					}
				System.out.println();
			}
		}
	}
}