import java.util.*;

public class Main{


	public static void main(String[] args) {
		int[] labels = MnistReader.getLabels("train-labels.idx1-ubyte");
		List<int[][]> images = MnistReader.getImages("train-images.idx3-ubyte");

		float[][] processedImages = processImages(images);
		float[][] processedLabels = processLabels(labels);

		//printImage(processedImages);
		NeurNet model = new NeurNet();
		model.addLayer(new Layer(processedImages[0].length));
		model.addLayer(new Layer(16));
		model.addLayer(new Layer(10));

		model.reset();



		//model.run(processedImages[0]);
		model.learn(processedImages, processedLabels);

		model.predict(processedImages[0]);

		Node[] output = model.layers.get(2).nodes;
		for(Node node : output)
			System.out.println(node.val + " ");
		System.out.println();

		System.out.println("I predict " + model.predict(processedImages[0]) + " actual value is " + labels[0]);
		System.out.println(MnistReader.renderImage(images.get(0)));
		//model.print();

		int accuracy = 0;
		for(int i = 0; i < processedImages.length; i++){
			if(model.predict(processedImages[i]) == labels[i])
				accuracy++;
		}
		System.out.println("accuracy: " + (accuracy*100)/processedImages.length);

	}

	public static float[][] processImages(List<int[][]> images){
		int totalImages = images.size();
		int imageRes = images.get(0).length * images.get(0)[0].length;

		float[][] output = new float[totalImages][imageRes];
		int m = 0;
		for(int i = 0; i < totalImages; i++){
			int[][] image = images.get(i);
			m = 0;
			for(int j = 0; j < image.length; j++){
				for(int k = 0; k < image[j].length; k++){

					output[i][m] = ((float)image[j][k])/(float)255.0;
					m++;

				}

			}
		}
		return output;
	}

	public static float[][] processLabels(int[] labels){
		float[][] processedLabels = new float[labels.length][10];
		for(int i = 0; i < labels.length; i++){
			processedLabels[i][labels[i]] = (float)1.0;
		}
		return processedLabels;
	}


	public static void printImage(float[][] image){
		for(int i = 0; i < image.length; i++){
			for(int j = 0; j < image[0].length; j++)
				System.out.print(image[i][j] + " ");
		}
		System.out.println();
	}
}

